import firebase from 'firebase/app'
import "firebase/auth";
import "firebase/firestore";
import "firebase/database";
import "firebase/storage";

const firebaseConfig = {
    databaseURL: "https://beeside-7f843-default-rtdb.europe-west1.firebasedatabase.app",
    apiKey: "AIzaSyCAGauAoCNXNHajBt7awDDPFZchGEXmyMg",
    authDomain: "beeside-7f843.firebaseapp.com",
    projectId: "beeside-7f843",
    storageBucket: "beeside-7f843.appspot.com",
    messagingSenderId: "560740979564",
    appId: "1:560740979564:web:246563cd3342de95139949"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export default firebase;
// export default fireDb.database().ref();
