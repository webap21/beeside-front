import React from 'react';
import {Box, Button, Container, Grid,} from '@material-ui/core';
import './style.css';
import { useHistory } from 'react-router-dom';
import particulier from "../../assets/images/particulier.svg";
import entreprise from "../../assets/images/entreprise.svg";

export default function RegisterChoices() {
    let history = useHistory();

    const handlePart = () => {
        history.push('/register')
    }

    const handlePro = () => {
        history.push('/register-pro')
    }

    return (
        <div className="wrapper register-choice">
            <div className="wrapper__box padding-l-10">
                <Grid container>
                    <Grid item md={12}>
                        <h1 className="title-blue">Welcome</h1>
                        <p className="text-blue">Is an alternative investment product on digital assets.</p>
                    </Grid>
                </Grid>
                <Grid container>
                    <Grid item md={12} className="text-center">
                        <h3>I am an</h3>
                    </Grid>
                </Grid>
                <Grid container spacing={3} >
                        <Grid item xs={6} sm={6} md={6} lg={6} xl={6} className="text-center">
                            <div className="border-grey">
                                <img className="img-selection" src={particulier} alt=""/>
                            </div>
                            <Button variant="contained" onClick={handlePart} color="info" class="btn-grey m-top-20">
                                Particulier
                            </Button>
                        </Grid>
                        <Grid item xs={6} sm={6} md={6} lg={6} xl={6} className="text-center">
                            <div className="border-grey">
                                <img className="img-selection" src={entreprise} alt=""/>
                            </div>
                            <Button variant="contained" onClick={handlePro} color="info" class="btn-grey m-top-20">
                                Institutionnel
                            </Button>
                        </Grid>
                </Grid>
            </div>

        </div>
    )
}
