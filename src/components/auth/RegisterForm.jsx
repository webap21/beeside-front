import React from 'react'
import FormTitle from './FormTitle';
import { useHistory } from 'react-router-dom';
import { Box, Button, TextField, Container, Checkbox, FormControlLabel, Modal, Backdrop } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import './style.css'
import Label from './label';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import InputAdornment from '@material-ui/core/InputAdornment';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import IconButton from '@material-ui/core/IconButton';
import FurtherInformation from "./FurtherInformation";
import firebase from "../../Firebase/initializeFirebase";
import Animate from 'react-smooth';
import StepperVertical from "./StepperVertical";
import pdfImg from "../../assets/images/pdf-gris.svg";
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import Fade from '@material-ui/core/Fade';
import Hidden from "@material-ui/core/Hidden";
import ReactTypingEffect from 'react-typing-effect';
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";


export default function RegisterForm(props) {

    const history = useHistory();

    const [emailFormView, setState] = React.useState(false);
    const [emailError, setEmailError, isNextField, passwordError, setPasswordError] = React.useState(null);
    const [showNext, setShowNext] = React.useState(0)

    const [values, setValues] = React.useState({
        email: '',
        password: '',
        confirmPassword: '',
        last_name: '',
        first_name: '',
        showPassword: false,
    });
    const [confirm, setConfirmAgree] = React.useState(false);
    const [imageAsUrl, setImageAsUrl] = React.useState();
    const [imageAsFile, setImageAsFile] = React.useState(null);
    const [name, setImageName] = React.useState(null);
    const move = 'ease-in';
    let [srcIframe, setSrcIframe] = React.useState();
    const [open, setOpen] = React.useState(false);
    const [showLastName, setLastName] = React.useState(false);
    const [showPasswordField, setPasswordField] = React.useState(false);
    const [showRetypePasswordField, setRetypePassword] = React.useState(false);
    const arrayChoices = [];
    const [political, setPolitical] = React.useState('');


    const handleImage = (event) => {
        const image = event.target.files[0]
        // console.log(image)
        setImageName(image.name)
        setImageAsFile(imageFile => (image))

    }

    const agree = (value) => {
        if(value.password === value.confirmPassword){
            firebase.auth().createUserWithEmailAndPassword(value.email, value.password)
                .then((userCredential) => {
                    const ref = firebase.firestore().collection('users');
                    var user = firebase.auth().currentUser;
                    const storage = firebase.storage();

                    if (imageAsFile === null) {
                        // console.error(`not an image, the image file is a ${typeof (imageAsFile)}`)
                        const res = ref.doc(user.uid).set({
                            last_name: value.last_name,
                            first_name: value.first_name,
                            email: user.email,
                        });
                    }
                    else {
                        const uploadTask = storage.ref(`/images/${imageAsFile.name}`).put(imageAsFile)
                        //initiates the firebase side uploading
                        uploadTask.on('state_changed',
                            (snapShot) => {
                            }, (err) => {
                                //catches the errors
                                console.log(err)
                            }, async () => {
                                const img = await storage.ref('images').child(imageAsFile.name).getDownloadURL();
                                const res = ref.doc(user.uid).set({
                                    last_name: value.last_name,
                                    first_name: value.first_name,

                                    email: user.email,
                                    profile_image: img,
                                });
                            })
                    }
                    setConfirmAgree(true)
                    setState(false)
                })
                .catch((error) => {
                    setConfirmAgree(false)
                    setState(true)
                    setEmailError(error.message)

                });
        }else{
            // setPasswordError("Passwords don't match");
        }
        //setConfirmAgree(true)
        //setState(false)

    };
    const handleChange = (prop) => (event) => {
        setEmailError(null)
        setValues({ ...values, [prop]: event.target.value });
    };

    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };
    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    const handleClickShowConfirmPassword = () => {
        setValues({ ...values, showConfirmPassword: !values.showConfirmPassword });
    };
    const handleMouseDownConfirmPassword = (event) => {
        event.preventDefault();
    };

    const setIframe = () => {
        fetch(`http://localhost:8080/api/get-der`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ email:values.email,first_name:values.first_name,
                last_name:values.last_name})
        })
            .then(response => response.json())
            .then(data => setSrcIframe(data));
    };

    const onClick = () => {
        if(showNext === 2){
            history.push('/futher');
        }else{
            setShowNext(showNext+1);
        }

        //check if current page is the one
        /*if(showNext === 1){
            setIframe();
        }*/

    }

    const handleOpen = () => {
      setOpen(true);
      setIframe();
    };

    const handleClose = () => {
        setOpen(false);
    };

    const showNextInputLastName = () => {
        setLastName(true);
    }

    const showNextInputPassword = () => {
        setPasswordField(true);
    }

    const showNextInputRetypePassword = () => {
        setRetypePassword(true);
    }

    const selectedChoice = (condition) => {
        arrayChoices.push(condition);
    }

    const nextPolitically = () => {
        if(political !== 'ok'){
            history.push("/welcome");
        }else{
            onClick();
        }

    }

    const handlePolitical = (event) => {
        setPolitical(event.target.value);
    };

    // console.log(values);

    return (
        <div className="form__wrapper">

            <div>
                {
                    confirm ?
                        <FurtherInformation first_name={values.first_name}
                                            last_name={values.last_name}/>
                        :
                        <Grid container direction="row" spacing={6}>
                            <Hidden xsDown>
                                <Grid item xs={6} className={'vertical-stepper'}>
                                    <StepperVertical/>
                                </Grid>
                            </Hidden>
                            <Grid item xs={12} sm={6} md={6} lg={6} xl={6}>
                                <div>

                                    {showNext === 0 &&
                                    <Animate easing={move} from={{opacity: 0}} to={{opacity: 1}}>
                                        {({opacity}) => (
                                            <div style={{opacity}}>
                                                <Box component="div" className="form__group">
                                                    <h3>Do you fall into any of the following categories?</h3>
                                                    <form>
                                                        <Grid container spacing={2} classeName="container-buttons">
                                                            <Grid item md={12} className="col-buttons">
                                                                <RadioGroup className="custom-radio" aria-label="political" name="political" value={political} onChange={handlePolitical}>
                                                                    <FormControlLabel value="exposed" control={<Radio />} label="I am politically exposed or a lobbyist" />
                                                                    <FormControlLabel value="spouse" control={<Radio />} label="My spouse is politically exposed or is part of a lobby" />
                                                                    <FormControlLabel value="close" control={<Radio />} label="Someone close to me is politically exposed or is part of a lobby" />
                                                                    <FormControlLabel value="ok" control={<Radio />} label="None of the above" />
                                                                </RadioGroup>
                                                            </Grid>
                                                        </Grid>
                                                        <Grid item md={12} sm={12} xs={12}>
                                                            <Animate easing={move} from={{opacity: 0}} to={{opacity: 1}}>
                                                                {({opacity}) => (
                                                                    <div style={{opacity}}>
                                                                        <div className="submission_block">
                                                                            <Button variant="contained" color="info"
                                                                                    class="question_btn next"
                                                                                    onClick={() => { nextPolitically(); }}>
                                                                                Next
                                                                            </Button>
                                                                        </div>
                                                                    </div>
                                                                )}
                                                            </Animate>
                                                        </Grid>
                                                    </form>
                                                </Box>
                                            </div>
                                        )}
                                    </Animate>
                                    }
                                    {showNext === 1 &&
                                    <Animate easing={move} from={{opacity: 0}} to={{opacity: 1}}>
                                        {({opacity}) => (
                                            <div style={{opacity}}>
                                                <ReactTypingEffect
                                                    text={"Firstly, we would need to create your account."}
                                                    cursorRenderer={cursor => <span></span>}
                                                    typingDelay={500}
                                                    speed={100}
                                                    eraseDelay={200000}
                                                    displayTextRenderer={(text, i) => {
                                                        return (
                                                            <h3>
                                                                {text.split('').map((char, i) => {
                                                                    return (
                                                                        <span>{char}</span>
                                                                    );
                                                                })}
                                                            </h3>
                                                        );
                                                    }}
                                                />
                                                <Box component="div" className="form__group">
                                                    <form>
                                                        <Grid container spacing={2}>
                                                            <Grid item md={12} sm={12} xs={12}>
                                                                <div className="input__field_block_signup">
                                                                    <Label/>
                                                                    <TextField size="medium"
                                                                            placeholder="Email Address"
                                                                            value={values.email}
                                                                            name="Email"
                                                                            type="Email"
                                                                            variant="outlined"
                                                                            onChange={handleChange('email')}
                                                                            onKeyDown={showNextInputPassword}
                                                                            className="field"/>
                                                                </div>
                                                                <p style={{
                                                                    fontWeight: 500,
                                                                    color: "red"
                                                                }}>{emailError}</p>
                                                            </Grid>
                                                            <Grid item md={12} sm={12} xs={12}>
                                                                {
                                                                    showPasswordField ?
                                                                        <Animate easing={move} from={{opacity: 0}} to={{opacity: 1}}>
                                                                            {({opacity}) => (
                                                                                <div style={{opacity}}>
                                                                                    <div className="input__field_block_signup">
                                                                                        <FormControl>
                                                                                            <Label/>
                                                                                            <Input
                                                                                                placeholder="Password"
                                                                                                className="standard-adornment-password"
                                                                                                type={values.showPassword ? 'text' : 'password'}
                                                                                                value={values.password}
                                                                                                onChange={handleChange('password')}
                                                                                                onKeyDown={showNextInputRetypePassword}
                                                                                                endAdornment={
                                                                                                    <InputAdornment position="end">
                                                                                                        <IconButton
                                                                                                            aria-label="toggle password visibility"
                                                                                                            onClick={handleClickShowPassword}
                                                                                                            onMouseDown={handleMouseDownPassword}
                                                                                                        >
                                                                                                            {values.showPassword ?
                                                                                                                <Visibility/> :
                                                                                                                <VisibilityOff/>}
                                                                                                        </IconButton>
                                                                                                    </InputAdornment>
                                                                                                }
                                                                                            />
                                                                                        </FormControl>
                                                                                    </div>
                                                                                </div>
                                                                            )}
                                                                        </Animate>
                                                                        :
                                                                        null
                                                                }

                                                            </Grid>
                                                            <Grid item md={12} sm={12} xs={12}>
                                                                {
                                                                    showRetypePasswordField ?
                                                                        <Animate easing={move} from={{opacity: 0}} to={{opacity: 1}}>
                                                                            {({opacity}) => (
                                                                                <div style={{opacity}}>
                                                                                    <div className="input__field_block_signup">
                                                                                        <FormControl>
                                                                                            <Label/>
                                                                                            <Input
                                                                                                placeholder="Retype password"
                                                                                                className="standard-adornment-password"
                                                                                                type={values.showConfirmPassword ? 'text' : 'password'}
                                                                                                value={values.confirmPassword}
                                                                                                onChange={handleChange('confirmPassword')}
                                                                                                endAdornment={
                                                                                                    <InputAdornment position="end">
                                                                                                        <IconButton
                                                                                                            aria-label="toggle password visibility"
                                                                                                            onClick={handleClickShowConfirmPassword}
                                                                                                            onMouseDown={handleMouseDownConfirmPassword}
                                                                                                        >
                                                                                                            {values.showConfirmPassword ?
                                                                                                                <Visibility/> :
                                                                                                                <VisibilityOff/>}
                                                                                                        </IconButton>
                                                                                                    </InputAdornment>
                                                                                                }
                                                                                            />
                                                                                        </FormControl>
                                                                                        <label>
                                                                                            <input name="isGoing" type="checkbox"/>
                                                                                        </label>
                                                                                        <span class="agree">I agree with</span> <a href="#">Terms & Conditions of Beeside</a>
                                                                                    </div>
                                                                                </div>
                                                                            )}
                                                                        </Animate>
                                                                        :
                                                                        null
                                                                }

                                                            </Grid>
                                                            {/*<Grid item md={12} sm={12} xs={12} >
                                                                <label
                                                                    className="btn_style custom-file-upload">
                                                                    <input type="file" onChange={handleImage} />
                                                                    <i className="fa fa-cloud-upload" /> Upload Image
                                                                </label>
                                                                <div className="file-preview">{name}</div>

                                                            </Grid>

                                                            <Grid item md={12} sm={12} xs={12}>
                                                                <EmailFormFooter error={emailError} furtherInfo={() => agree(values)} />
                                                            </Grid>*/}
                                                            <Grid item md={12} sm={12} xs={12}>
                                                                {
                                                                    showRetypePasswordField ?
                                                                        <div className="submission_block">
                                                                            <Button variant="contained" color="info"
                                                                                    class="question_btn next"
                                                                                    onClick={onClick}>
                                                                                Next
                                                                            </Button>
                                                                        </div>
                                                                        :
                                                                        null
                                                                }

                                                            </Grid>
                                                        </Grid>
                                                    </form>
                                                </Box>
                                            </div>
                                        )}
                                    </Animate>
                                    }
                                    {showNext === 2 &&
                                    <Animate easing={move} from={{opacity: 0}} to={{opacity: 1}}>
                                        {({opacity}) => (
                                            <div style={{opacity}}>
                                                <div class="blue">
                                                    <h3>Great,<br></br>it's time to get to know each other better</h3>
                                                </div>
                                                <ReactTypingEffect
                                                    text={"What is your name?"}
                                                    cursorRenderer={cursor => <span></span>}
                                                    typingDelay={500}
                                                    speed={100}
                                                    eraseDelay={200000}
                                                    displayTextRenderer={(text, i) => {
                                                        return (
                                                            <h3>
                                                                {text.split('').map((char, i) => {
                                                                    return (
                                                                        <span>{char}</span>
                                                                    );
                                                                })}
                                                            </h3>
                                                        );
                                                    }}
                                                />
                                                <Box component="div" className="form__group">
                                                    <form>
                                                        <Grid container spacing={2}>
                                                            <Grid item md={12} sm={12} xs={12}>
                                                                <div className="input__field_block_signup">
                                                                    <TextField size="medium"
                                                                               placeholder="Firstname"
                                                                               name="first_name"
                                                                               value={values.first_name}
                                                                               variant="outlined"
                                                                               onChange={handleChange('first_name')}
                                                                               onKeyDown={showNextInputLastName}
                                                                               className="field"/>
                                                                </div>
                                                            </Grid>
                                                            <Grid item md={12} sm={12} xs={12}>
                                                                {
                                                                    showLastName &&
                                                                        <Animate easing={move} from={{opacity: 0}} to={{opacity: 1}}>
                                                                            {({opacity}) => (
                                                                                <div style={{opacity}}>
                                                                                    <div className="input__field_block_signup">
                                                                                        <TextField size="medium"
                                                                                                   name="last_name"
                                                                                                   placeholder="Last Name"
                                                                                                   variant="outlined"
                                                                                                   value={values.last_name}
                                                                                                   onChange={handleChange('last_name')}
                                                                                                   className="field"/>
                                                                                    </div>
                                                                                </div>
                                                                            )}
                                                                        </Animate>
                                                                }

                                                            </Grid>
                                                            <Grid item md={6} sm={6} xs={6}>
                                                                {
                                                                    showLastName &&
                                                                        <Animate easing={move} from={{opacity: 0}} to={{opacity: 1}}>
                                                                            {({opacity}) => (
                                                                                <div style={{opacity}}>
                                                                                    <div className="submission_block">
                                                                                        <Button variant="contained" color="info"
                                                                                                class="question_btn previous"
                                                                                                onClick={onClick}>
                                                                                            Previous
                                                                                        </Button>
                                                                                    </div>
                                                                                </div>
                                                                            )}
                                                                        </Animate>
                                                                }
                                                            </Grid>
                                                            <Grid item md={6} sm={6} xs={6}>
                                                                {
                                                                    showLastName &&
                                                                        <Animate easing={move} from={{opacity: 0}} to={{opacity: 1}}>
                                                                            {({opacity}) => (
                                                                                <div style={{opacity}}>
                                                                                    <div className="submission_block">
                                                                                        <Button variant="contained" color="info"
                                                                                                class="question_btn next"
                                                                                                onClick={onClick}>
                                                                                            Next
                                                                                        </Button>
                                                                                    </div>
                                                                                </div>
                                                                            )}
                                                                        </Animate>
                                                                }
                                                            </Grid>
                                                        </Grid>
                                                    </form>

                                                </Box>
                                            </div>
                                        )}
                                    </Animate>
                                    }
                                    {/*{showNext === 3 &&
                                    <Animate easing={move} from={{opacity: 0}} to={{opacity: 1}}>
                                        {({opacity}) => (
                                            <div style={{opacity}}>
                                                <Box component="div" className="form__group">
                                                    <FormTitle sign_up_title={<span>To complete this step, we need you to sign the following online document</span>}/>
                                                    <form>
                                                        <Grid container spacing={2} className="align-flex-center">
                                                            <Grid item md={12} sm={12} xs={12}>
                                                                <p className="enumerate"><span>1</span> - Open the document</p>
                                                            </Grid>
                                                            <Grid item md={12} sm={12} xs={12}>
                                                                <p className="enumerate"><span>2</span> - Electronic signature on pdf</p>
                                                            </Grid>
                                                            <Grid item md={12} sm={12} xs={12}>
                                                                <Button className="doc-download" endIcon={<ArrowForwardIosIcon />} onClick={handleOpen}>
                                                                    <img src={pdfImg} alt=""/> <span>Open the document</span>
                                                                </Button>
                                                            </Grid>
                                                            <Modal
                                                                open={open}
                                                                onClose={handleClose}
                                                            >
                                                                <Fade in={open}>
                                                                    <div>
                                                                        {
                                                                            srcIframe ?
                                                                                <iframe src={`https://staging-app.yousign.com/procedure/sign?members=${srcIframe.members}&signatureUi=/signature_uis/f1bdb91d-e698-425d-b7f4-4ec88552cff0`}></iframe>
                                                                                :
                                                                                <span></span>
                                                                        }
                                                                    </div>
                                                                </Fade>
                                                            </Modal>
                                                            <Grid item md={12} sm={12} xs={12}>
                                                                <div className="submission_block">
                                                                    <Button variant="contained" color="info"
                                                                            class="question_btn"
                                                                            onClick={() => agree(values)}>
                                                                        Confirm
                                                                    </Button>
                                                                </div>
                                                            </Grid>
                                                        </Grid>
                                                    </form>

                                                </Box>
                                            </div>
                                        )}
                                    </Animate>
                                    }*/}
                                </div>
                            </Grid>
                        </Grid>
                    }
            </div>
        </div>
    )
}
