import React from 'react'
import FormTitle from './FormTitle';
import {Box, Button, Checkbox, FormControlLabel, Modal, TextField} from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import './style.css'
import Label from './label';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import InputAdornment from '@material-ui/core/InputAdornment';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import IconButton from '@material-ui/core/IconButton';
import firebase from "../../Firebase/initializeFirebase";
import Animate from 'react-smooth';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import StepContent from "@material-ui/core/StepContent";
import Stepper from "@material-ui/core/Stepper";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import pdfImg from "../../assets/images/pdf-gris.svg";
import emailImg from "../../assets/images/emailsent.svg";
import Fade from "@material-ui/core/Fade";
import Hidden from "@material-ui/core/Hidden";

function getSteps() {
    return ['My account', 'Validation'];
}

function getStepContent(step) {
    switch (step) {
        case 0:
            return `Step 1`;
        case 1:
            return 'Step 2';
        default:
            return 'Unknown step';
    }
}

export default function RegisterFormPro(props) {

    /****
    Stepper vertical
    **/
    const [activeStep, setActiveStep] = React.useState(0);
    const steps = getSteps();

    const handleNext = () => {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    /****
     Fin Stepper vertical
     **/

    const [emailFormView, setState] = React.useState(false);
    const [emailError, setEmailError, isNextField, passwordError, setPasswordError] = React.useState(null);
    const [showNext, setShowNext] = React.useState(0)

    const [values, setValues] = React.useState({
        email: '',
        password: '',
        confirmPassword: '',
        last_name: '',
        first_name: '',
        company: '',
        showPassword: false,
    });
    const [confirm, setConfirmAgree] = React.useState(false);
    const [imageAsUrl, setImageAsUrl] = React.useState();
    const [imageAsFile, setImageAsFile] = React.useState(null);
    const [name, setImageName] = React.useState(null);
    const move = 'ease-in';
    let [srcIframe, setSrcIframe] = React.useState();
    const [open, setOpen] = React.useState(false);
    const [showLastName, setLastName] = React.useState(false);
    const [showFirstNameField, setFirstNameField] = React.useState(false);
    const [showPasswordField, setPasswordField] = React.useState(false);
    const [showRetypePasswordField, setRetypePassword] = React.useState(false);
    const [showEmailField, setEmailField] = React.useState(false);
    const [showCompanyField, setCompanyField] = React.useState(true);
    const [showDocField, setDocField] = React.useState(false);
    const [showEmailSent, setEmailSent] = React.useState(false);

    const agree = (value) => {
        /*if(value.password === value.confirmPassword){
            firebase.auth().createUserWithEmailAndPassword(value.email, value.password)
                .then((userCredential) => {
                    const ref = firebase.firestore().collection('users');
                    var user = firebase.auth().currentUser;
                    const storage = firebase.storage();
                    const res = ref.doc(user.uid).set({
                        company: value.company,
                        email: user.email,
                        last_name: value.last_name,
                        first_name: value.first_name,
                    });

                    setConfirmAgree(true)
                    setState(false)
                })
                .catch((error) => {
                    setConfirmAgree(false)
                    setState(true)
                    setEmailError(error.message)
                });
        }else{
            // setPasswordError("Passwords don't match");
        }*/
        //setConfirmAgree(true)
        //setState(false)
        setDocField(false);
        handleNext();
        setEmailSent(true);

    };

    const setIframe = () => {
        /*fetch(`http://localhost:8080/api/get-der`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ email:values.email,first_name:values.first_name,
                last_name:values.last_name})
        })
            .then(response => response.json())
            .then(data => setSrcIframe(data));*/
    };

    const handleOpen = () => {
        setOpen(true);
        setIframe();
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleChange = (prop) => (event) => {
        setEmailError(null)
        setValues({ ...values, [prop]: event.target.value });
    };

    const onClick = () => {
        setShowNext(showNext+1);
    }

    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };
    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    const handleClickShowConfirmPassword = () => {
        setValues({ ...values, showConfirmPassword: !values.showConfirmPassword });
    };
    const handleMouseDownConfirmPassword = (event) => {
        event.preventDefault();
    };


    const showNextInputLastName = () => {
        setLastName(true);
    }

    const showNextInputFirstName = () => {
        setCompanyField(false);
        setFirstNameField(true);
    }

    const showNextInputPassword = () => {
        setPasswordField(true);
    }

    const showNextInputEmail = () => {
        setFirstNameField(false);
        setEmailField(true);
    }

    const showNextDocField = () => {
        setEmailField(false);
        setDocField(true);
    }

    const showNextInputRetypePassword = () => {
        setRetypePassword(true);
    }

    const handleClickOpen = () => {
        setOpen(true);
    };

    // console.log(values);

    return (
        <div className="form__wrapper register-pro">
            <div>
                <Grid container direction="row" spacing={6}>
                    <Hidden xsDown>
                        <Grid item xs={6} className={'vertical-stepper'}>
                            <div>
                                <h2 className="title-white">Create account</h2>
                                <Stepper activeStep={activeStep} orientation="vertical">
                                    {steps.map((label, index) => (
                                        <Step key={label}>
                                            <StepLabel>{label}</StepLabel>
                                            <StepContent>
                                                {getStepContent(index)}
                                            </StepContent>
                                        </Step>
                                    ))}
                                </Stepper>
                            </div>
                        </Grid>
                    </Hidden>
                    <Grid item md={6}>
                        <div>
                            {
                                showCompanyField &&
                                <Animate easing={move} from={{opacity: 0}} to={{opacity: 1}}>
                                    {({opacity}) => (
                                        <div style={{opacity}}>
                                            <Box component="div" className="form__group">
                                                <h2>First</h2>
                                                <h3>What is the company's name?</h3>
                                                <form>
                                                    <Grid container spacing={2}>
                                                        <Grid item md={12} sm={12} xs={12}>
                                                            <div className="input__field_block_signup">
                                                                <Label/>
                                                                <TextField size="medium"
                                                                           name="company"
                                                                           placeholder="Name of the company"
                                                                           value={values.company}
                                                                           variant="outlined"
                                                                           onChange={handleChange('company')}
                                                                           className="field"/>
                                                            </div>
                                                        </Grid>
                                                        <Grid item md={6} sm={6} xs={6}>
                                                            <div className="submission_block">
                                                                <Button variant="contained" color="info"
                                                                        class="question_btn previous"
                                                                        onClick={showNextInputFirstName}>
                                                                    Previous
                                                                </Button>
                                                            </div>
                                                        </Grid>
                                                        <Grid item md={6} sm={6} xs={6}>
                                                            <div className="submission_block">
                                                                <Button variant="contained" color="info"
                                                                        class="question_btn next"
                                                                        onClick={showNextInputFirstName}>
                                                                    Next
                                                                </Button>
                                                            </div>
                                                        </Grid>
                                                    </Grid>
                                                </form>
                                            </Box>
                                        </div>
                                    )}
                                </Animate>
                            }
                            <Box component="div" className="form__group">
                                <Grid item md={12} sm={12} xs={12}>
                                    {
                                        showFirstNameField &&
                                            <Animate easing={move} from={{opacity: 0}} to={{opacity: 1}}>
                                                {({opacity}) => (
                                                    <div style={{opacity}}>
                                                        <h2>Secondly</h2>
                                                        <h3>We need your names</h3>
                                                        <Grid container spacing={2}>
                                                            <Grid item md={12} sm={12} xs={12}>
                                                                <div className="input__field_block_signup">
                                                                    <Label/>
                                                                    <TextField size="medium"
                                                                               name="first_name"
                                                                               placeholder="Surname"
                                                                               value={values.first_name}
                                                                               variant="outlined"
                                                                               onChange={handleChange('first_name')}
                                                                               className="field"/>
                                                                </div>
                                                                <div className="input__field_block_signup">
                                                                    <Label/>
                                                                    <TextField size="medium"
                                                                               name="last_name"
                                                                               placeholder="Name"
                                                                               variant="outlined"
                                                                               value={values.last_name}
                                                                               onChange={handleChange('last_name')}
                                                                               className="field"/>
                                                                </div>
                                                            </Grid>
                                                            <Grid item md={6} sm={6} xs={6}>
                                                                <div className="submission_block">
                                                                    <Button variant="contained" color="info"
                                                                            class="question_btn previous"
                                                                            onClick={showNextInputEmail}>
                                                                        Previous
                                                                    </Button>
                                                                </div>
                                                            </Grid>
                                                            <Grid item md={6} sm={6} xs={6}>
                                                                <div className="submission_block">
                                                                    <Button variant="contained" color="info"
                                                                            class="question_btn next"
                                                                            onClick={showNextInputEmail}>
                                                                        Next
                                                                    </Button>
                                                                </div>
                                                            </Grid>
                                                        </Grid>
                                                    </div>
                                                )}
                                            </Animate>
                                    }

                                </Grid>
                                <Grid item md={12} sm={12} xs={12}>
                                    {
                                        showEmailField &&
                                            <Animate easing={move} from={{opacity: 0}} to={{opacity: 1}}>
                                                {({opacity}) => (
                                                    <div style={{opacity}}>
                                                        <h2>And</h2>
                                                        <h3>to finalise your account</h3>
                                                        <Grid container spacing={2}>
                                                            <Grid item md={12} sm={12} xs={12}>
                                                                <div className="input__field_block_signup">
                                                                    <Label/>
                                                                    <TextField size="medium"
                                                                               value={values.email}
                                                                               name="Email"
                                                                               placeholder="Email Address"
                                                                               type="Email"
                                                                               variant="outlined"
                                                                               onChange={handleChange('email')}
                                                                               onKeyDown={showNextInputPassword}
                                                                               className="field"/>
                                                                </div>
                                                                <p style={{
                                                                    fontWeight: 500,
                                                                    color: "red"
                                                                }}>{emailError}</p>
                                                            </Grid>
                                                            {
                                                                showPasswordField &&
                                                                <Grid item md={12} sm={12} xs={12}>
                                                                    <Animate easing={move} from={{opacity: 0}}
                                                                             to={{opacity: 1}}>
                                                                        {({opacity}) => (
                                                                            <div style={{opacity}}>
                                                                                    <div
                                                                                        className="input__field_block_signup">
                                                                                        <FormControl>
                                                                                            <Label/>
                                                                                            <Input
                                                                                                placeholder="Password"
                                                                                                id="standard-adornment-password"
                                                                                                type={values.showPassword ? 'text' : 'password'}
                                                                                                value={values.password}
                                                                                                onChange={handleChange('password')}
                                                                                                onKeyDown={showNextInputRetypePassword}
                                                                                                endAdornment={
                                                                                                    <InputAdornment
                                                                                                        position="end">
                                                                                                        <IconButton
                                                                                                            aria-label="toggle password visibility"
                                                                                                            onClick={handleClickShowPassword}
                                                                                                            onMouseDown={handleMouseDownPassword}
                                                                                                        >
                                                                                                            {values.showPassword ?
                                                                                                                <Visibility/> :
                                                                                                                <VisibilityOff/>}
                                                                                                        </IconButton>
                                                                                                    </InputAdornment>
                                                                                                }
                                                                                            />
                                                                                        </FormControl>
                                                                                    </div>

                                                                            </div>
                                                                        )}
                                                                    </Animate>
                                                                </Grid>
                                                            }
                                                            {
                                                                showRetypePasswordField &&
                                                                <Grid item md={12} sm={12} xs={12}>
                                                                    <Animate easing={move} from={{opacity: 0}}
                                                                             to={{opacity: 1}}>
                                                                        {({opacity}) => (
                                                                            <div style={{opacity}}>

                                                                                    <div
                                                                                        className="input__field_block_signup">
                                                                                        <FormControl>
                                                                                            <Label/>
                                                                                            <Input
                                                                                                placeholder="Retype password"
                                                                                                id="standard-adornment-password"
                                                                                                type={values.showConfirmPassword ? 'text' : 'password'}
                                                                                                value={values.confirmPassword}
                                                                                                onChange={handleChange('confirmPassword')}
                                                                                                endAdornment={
                                                                                                    <InputAdornment
                                                                                                        position="end">
                                                                                                        <IconButton
                                                                                                            aria-label="toggle password visibility"
                                                                                                            onClick={handleClickShowConfirmPassword}
                                                                                                            onMouseDown={handleMouseDownConfirmPassword}
                                                                                                        >
                                                                                                            {values.showConfirmPassword ?
                                                                                                                <Visibility/> :
                                                                                                                <VisibilityOff/>}
                                                                                                        </IconButton>
                                                                                                    </InputAdornment>
                                                                                                }
                                                                                            />
                                                                                        </FormControl>

                                                                                    </div>

                                                                            </div>
                                                                        )}
                                                                    </Animate>
                                                                </Grid>
                                                            }
                                                            <Grid item md={6} sm={6} xs={6}>
                                                                <div className="submission_block">
                                                                    <Button variant="contained" color="info"
                                                                            class="question_btn previous"
                                                                            onClick={showNextInputEmail}>
                                                                        Previous
                                                                    </Button>
                                                                </div>
                                                            </Grid>
                                                            <Grid item md={6} sm={6} xs={6}>
                                                                <div className="submission_block">
                                                                    <Button variant="contained" color="info"
                                                                            class="question_btn next"
                                                                            onClick={showNextDocField}>
                                                                        Next
                                                                    </Button>
                                                                </div>
                                                            </Grid>
                                                        </Grid>
                                                    </div>
                                                    )}
                                            </Animate>

                                    }

                                </Grid>
                                {
                                    showDocField &&
                                    <Grid item md={12} sm={12} xs={12}>
                                        <Animate easing={move} from={{opacity: 0}} to={{opacity: 1}}>
                                            {({opacity}) => (
                                                <div style={{opacity}}>
                                                    <Box component="div" className="form__group last-step">
                                                        <h3>To complete this step, we need you to sign the following online document</h3>
                                                        <form>
                                                            <Grid container spacing={2} className="align-flex-center">
                                                                <Grid item md={12} sm={12} xs={12}>
                                                                    <p className="enumerate"><span class="one">1</span> - Open the document</p>
                                                                </Grid>
                                                                <Grid item md={12} sm={12} xs={12}>
                                                                    <p className="enumerate"><span class="two">2</span> - Electronic signature on pdf</p>
                                                                </Grid>
                                                                <Grid item md={12} sm={12} xs={12}>
                                                                    <Button className="doc-download" endIcon={<ArrowForwardIosIcon />} onClick={handleOpen}>
                                                                        <img src={pdfImg} alt=""/> <span>Open the document</span>
                                                                    </Button>
                                                                </Grid>
                                                                <Grid item md={12} sm={12} xs={12}>
                                                                    <a href="#">Download my contract</a>
                                                                </Grid>
                                                                <Grid item md={12} sm={12} xs={12}>
                                                                    <label>
                                                                        <input name="isGoing" type="checkbox"/>
                                                                    </label>
                                                                    <span class="agree">I agree with</span> <a href="#">Terms & Conditions of Beeside</a>
                                                                </Grid>
                                                                <Modal
                                                                    open={open}
                                                                    onClose={handleClose}
                                                                >
                                                                    <Fade in={open}>
                                                                        <div>
                                                                            {
                                                                                srcIframe ?
                                                                                    <iframe src={`https://staging-app.yousign.com/procedure/sign?members=${srcIframe.members}&signatureUi=/signature_uis/f1bdb91d-e698-425d-b7f4-4ec88552cff0`}></iframe>
                                                                                    :
                                                                                    <span></span>
                                                                            }
                                                                        </div>
                                                                    </Fade>
                                                                </Modal>
                                                                {/*<Grid item md={12} sm={12} xs={12}>
                                                            <div className="submission_block">
                                                                <Button variant="contained" color="info"
                                                                        class="question_btn"
                                                                        onClick={() => agree(values)}>
                                                                    Confirm
                                                                </Button>
                                                            </div>
                                                        </Grid>*/}
                                                            <Grid item md={6} sm={6} xs={6}>
                                                                <div className="submission_block">
                                                                    <Button variant="contained" color="info"
                                                                            class="question_btn previous"
                                                                            onClick={() => { agree(values); }}>
                                                                        Previous
                                                                    </Button>
                                                                </div>
                                                            </Grid>
                                                            <Grid item md={6} sm={6} xs={6}>
                                                                <div className="submission_block">
                                                                    <Button variant="contained" color="info"
                                                                            class="question_btn next"
                                                                            onClick={() => { agree(values); }}>
                                                                        Submit
                                                                    </Button>
                                                                </div>
                                                            </Grid>
                                                        </Grid>    
                                                        </form>

                                                    </Box>
                                                </div>

                                            )}
                                        </Animate>
                                    </Grid>
                                }

                                {
                                    showEmailSent &&
                                    <Animate easing={move} from={{opacity: 0}} to={{opacity: 1}}>
                                        {({opacity}) => (
                                            <div style={{opacity}}>
                                                <Box component="div" className="form__group">
                                                    <FormTitle sign_up_title={<span>Thanks,<br></br>An email has been sent to you to contact us directly.</span>}/>
                                                    <form>
                                                        <Grid container spacing={2}>
                                                            <Grid item md={12} sm={12} xs={12}>
                                                                <div className="input__field_block_signup text-center">
                                                                    <img src={emailImg} alt=""/>
                                                                </div>
                                                            </Grid>
                                                        </Grid>
                                                    </form>
                                                </Box>
                                            </div>
                                        )}
                                    </Animate>
                                }


                            </Box>
                        </div>
                        <Dialog
                            open={open}
                            aria-labelledby="responsive-dialog-title"
                        >
                            <DialogContent>
                                <DialogContentText>
                                    Lorem ipsum email
                                </DialogContentText>
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={handleClose} color="primary" autoFocus>
                                    OK
                                </Button>
                            </DialogActions>
                        </Dialog>
                    </Grid>
                </Grid>

            </div>
        </div>
    )
}
