import React from 'react'
import FormTitle from './FormTitle';
import { Box, Button, TextField, Container, Checkbox, FormControlLabel } from '@material-ui/core';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Grid from '@material-ui/core/Grid';
import './style.css'

function getSteps() {
    return ['General Information', 'Personal Information', 'Investor profile', 'Sources of revenue', 'Deposit'];
}

function getStepContent(step) {
    switch (step) {
        case 0:
            return `Step 1`;
        case 1:
            return 'Step 2';
        case 2:
            return `Step 3`;
        default:
            return 'Unknown step';
    }
}

export default function StepperVertical(props) {

    const [activeStep, setActiveStep] = React.useState(0);
    const steps = getSteps();

    const handleNext = () => {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    return (
        <div>
            <Stepper activeStep={activeStep} orientation="vertical">
                {steps.map((label, index) => (
                    <Step key={label}>
                        <StepLabel>{label}</StepLabel>
                        <StepContent>
                            {getStepContent(index)}
                        </StepContent>
                    </Step>
                ))}
            </Stepper>
        </div>
    );
}
