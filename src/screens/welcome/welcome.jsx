import React from 'react'
import { Box, Container, Grid, } from '@material-ui/core';
import './style.css';
import Hidden from '@material-ui/core/Hidden';
import WelcomeMobile from "../../components/Welcome/WelcomeMobile";
import WelcomeDesktop from "../../components/Welcome/WelcomeDesktop";

export default function Welcome() {
    const [open, setOpen] = React.useState(false);

    const handleOpen = () => {
        setOpen(true);
    };


    return (
        <div className="wrapper">
            <div className="wrapper__box">
                <Box component="div" className="box__content">
                    <Container fixed maxWidth="md" className="container" disableGutters>
                        <div >
                            <Hidden smUp>
                                <WelcomeMobile />
                            </Hidden>
                            <Hidden xsDown>
                                <WelcomeDesktop />
                            </Hidden>
                        </div>
                    </Container>
                </Box>
            </div>

        </div>
    )
}
